# Setup the most basic image
ARG BASEIMAGE=rootproject/root:6.22.00-ubuntu20.04
FROM ${BASEIMAGE}

CMD /bin/bash
WORKDIR /


### First install the dependencies
RUN apt-get update && \
    apt-get dist-upgrade -y &&  \
    apt-get upgrade -y &&  \
    apt-get install -y cmake g++ python3 python3-pip git sudo nano wget libopenblas-dev

### First install RooUnfold
RUN git clone https://gitlab.cern.ch/RooUnfold/RooUnfold.git && \
    mkdir build_roounfold && \
    cd build_roounfold && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local/RooUnFold ../RooUnfold/ &&\
    make -j3 > /dev/null && \
    sudo make install > /dev/null && \
    cd .. && \
    rm -rf build_roounfold RooUnfold
RUN  pip install numpy  && \
     pip install scipy  && \
     pip install scikit-learn && \
     pip install pandas  && \
     pip install matplotlib  && \
     pip install dwave-system && \
     pip install dwave-neal &&  \
     pip install dwave-preprocessing==0.4.0 --no-deps  && \
     pip install dwave-qbsolv==0.3.3 --no-deps && \
     pip install hyperopt && \
     pip install seaborn


     



#install dwave-qbsolv==0.3.3
# RUN pip install numpy==1.15.0 && \
#     pip install scipy==1.2.0 && \
#     pip install scikit-learn==0.19.2 && \
#     pip install pandas==0.23.4 && \

#     pip install dimod==0.8.12 && \
#     pip install dwave-system==0.7.0 && \
# (installed by dwave-system)    pip install dwave-networkx==0.8.0  && \
#     pip install dwave-neal==0.5.0 && \
#     pip install dwave-qbsolv==0.2.10 && \
#     pip install hyperopt==0.1.2 && \
#     pip install matplotlib==2.2.4  && \
#     pip install seaborn==0.9.0 




# minorminer
# seaborn


    #  && \
    # echo "#!/bin/bash" > ~/setup_docker.sh && \
    # echo "cd /usr/local/geant4/bin" >> ~/setup_docker.sh && \
    # echo "source geant4.sh" >> ~/setup_docker.sh && \
    # echo "cd -" >> ~/setup_docker.sh && \
    # sudo mv ~/setup_docker.sh  /usr/local/geant4/setup_docker.sh && \
    # echo "source /usr/local/geant4/setup_docker.sh" >> ~/.bashrc
